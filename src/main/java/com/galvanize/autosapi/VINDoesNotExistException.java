package com.galvanize.autosapi;

public class VINDoesNotExistException extends RuntimeException {
    public VINDoesNotExistException(String errorMessage) {
        super(errorMessage);
    }
}
