package com.galvanize.autosapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
import java.util.Optional;

@Service
public class AutosService {

    AutosRepository autosRepository;

    public AutosService (AutosRepository autosRepository) {
        this.autosRepository = autosRepository;
    }

    public AutosList getAutos() {
        return new AutosList(autosRepository.findAll());
    }

    public AutosList getAutos(String color, String make) {
        List<Automobile> automobiles = autosRepository.findByColorContainsAndMakeContains(color, make);
        if(!automobiles.isEmpty()){
            return new AutosList(automobiles);
        }
        return null;
    }


//    @RequestBody
    public Automobile addAuto(Automobile auto) {
        return autosRepository.save(auto);

    }

    public Automobile getAuto(String vin) throws VINDoesNotExistException {
        if (autosRepository.findByVin(vin).isPresent()) return autosRepository.findByVin(vin).get();
            throw new VINDoesNotExistException("VIN does not exist");
    }

    public Automobile updateAuto(String vin, String color, String owner) {
        Optional<Automobile> oAuto = autosRepository.findByVin(vin);
        if (oAuto.isPresent()) {
            oAuto.get().setColor(color);
            oAuto.get().setOwner(owner);
            return autosRepository.save(oAuto.get());
        }
        return null;
    }

    public void deleteAuto(String vin) {
        Optional<Automobile> oAuto = autosRepository.findByVin(vin);
        if(oAuto.isPresent()) {
            autosRepository.delete(oAuto.get());
        } else {
            throw new AutoNotFoundException();
        }
    }
}
