package com.galvanize.autosapi;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.json.JSONObject;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AutosApiApplicationTests {

    @Autowired
    TestRestTemplate restTemplate;
    private RestTemplate patchRestTemplate;

    @Autowired
    AutosRepository autosRepository;

    Random r = new Random();
    List<Automobile> testAutos;
    @BeforeEach
    void setUp() {
        this.patchRestTemplate = restTemplate.getRestTemplate();
        HttpClient httpClient = HttpClientBuilder.create().build();
        this.patchRestTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

        this.testAutos = new ArrayList<>();
        Automobile auto;
        String[] colors = {"RED", "BLUE", "GREEN", "ORANGE", "YELLOW", "BLACK", "BROWN", "ROOT BEER", "MAGENTA", "AMBER"};

        for (int i = 0; i < 50; i++) {
            if (i % 3 == 0) {
                auto = new Automobile(1967, "Ford", "Mustang", "AABBCC"+(i*13));
                auto.setColor(colors[r.nextInt(10)]);
            }else if (i % 2 == 0) {
                auto = new Automobile(2000, "Dodge", "Viper", "VVBBXX"+(i*12));
                auto.setColor(colors[r.nextInt(10)]);
            }else {
                auto = new Automobile(2020, "Audi", "Quatro", "QQZZAA" + (i * 12));
                auto.setColor(colors[r.nextInt(10)]);
            }
            this.testAutos.add(auto);
        }
        autosRepository.saveAll(this.testAutos);
	}

	@AfterEach
    void tearDown() {
        autosRepository.deleteAll();
    }

	@Test
	void contextLoads() {
		assertEquals(1, 1);
	}

	@Test
    void getAutos_exists_returnsAutosList() {
        ResponseEntity<AutosList> response = restTemplate.getForEntity("/api/autos", AutosList.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().isEmpty()).isFalse();
        for (Automobile auto : response.getBody().getAutomobiles()) {
            System.out.println(auto);
        }
    }

    @Test
    void getAutos_search_returnsAutosList() {
        int seq = r.nextInt(50);

        String color = testAutos.get(seq).getColor();
        String make = testAutos.get(seq).getMake();
        ResponseEntity<AutosList> response = restTemplate.getForEntity(
                String.format("/api/autos?color=%s&make=%s", color, make), AutosList.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().isEmpty()).isFalse();
        assertThat(response.getBody().getAutomobiles().size()).isGreaterThanOrEqualTo(1);
        for (Automobile auto : response.getBody().getAutomobiles()) {
            System.out.println(auto);
        }
    }

    @Test
    void addAuto_returnsNewAutoDetails() {
        Automobile automobile = new Automobile();
        automobile.setVin("ABC123XX");
        automobile.setYear(1995);
        automobile.setMake("Ford");
        automobile.setModel("Windstar");
        automobile.setColor("Blue");

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<Automobile> request = new HttpEntity<>(automobile, headers);

        ResponseEntity<Automobile> response = restTemplate.postForEntity("/api/autos", request, Automobile.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getVin()).isEqualTo(automobile.getVin());
    }

    @Test
    public void getAutoAndPatch() throws JSONException {
        Automobile automobile;
        automobile = testAutos.get(0);

        String resourceUrl = "/api/autos/" + automobile.getVin();

        JSONObject updateBody = new JSONObject();
        updateBody.put("color", "Platinum");
        updateBody.put("owner", "Ashton");

        ResponseEntity responseEntity =
                patchRestTemplate.exchange(resourceUrl, HttpMethod.PATCH, getPostRequestHeaders(updateBody.toString()), Automobile.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, responseEntity.getHeaders().getContentType());
        Automobile updatedAutomobile = (Automobile) responseEntity.getBody();
        assertEquals("Platinum", updatedAutomobile.getColor());
        assertEquals("Ashton", updatedAutomobile.getOwner());
    }

    public HttpEntity getPostRequestHeaders(String jsonPostBody) {
        List acceptTypes = new ArrayList();
        acceptTypes.add(MediaType.APPLICATION_JSON);

        HttpHeaders reqHeaders = new HttpHeaders();
        reqHeaders.setContentType(MediaType.APPLICATION_JSON);
        reqHeaders.setAccept(acceptTypes);

        return new HttpEntity(jsonPostBody, reqHeaders);
    }

    @Test
    void deleteAuto_returnsNoContentStatus204() {
        Automobile automobile;
        automobile = testAutos.get(0);

        String resourceUrl = "/api/autos/" + automobile.getVin();

        patchRestTemplate.delete(resourceUrl);

        ResponseEntity<Automobile> responseEntity = restTemplate.getForEntity(
                String.format("/api/autos/" + automobile.getVin()), Automobile.class);

        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
    }

    @Test
    public void autoDoesNotExist_returnsNoContent() {
        ResponseEntity<Automobile> responseEntity = restTemplate.getForEntity("/api/autos/1234567", Automobile.class);
        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
    }
}
